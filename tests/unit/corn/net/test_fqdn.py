import idem_windows.corn.net.fqdn
import pytest
import socket
import unittest.mock as mock


class TestFqdn:
    @pytest.mark.asyncio
    async def test_load_localhost(self, c_hub):
        with mock.patch.object(socket, "gethostname", return_value="test_host"):
            await idem_windows.corn.net.fqdn.load_localhost(c_hub)

        assert c_hub.corn.CORN.localhost == "test_host"

    @pytest.mark.asyncio
    async def test_load_fqdn(self, c_hub):
        with mock.patch.object(
            socket,
            "getaddrinfo",
            side_effect=[
                [],
                [(socket.AF_INET, 0, 0, "", ("192.168.1.2", 0))],
                [(socket.AF_INET6, 0, 0, "", ("fe80::71a9:ffff:ffff:ffff", 0, 0, 19))],
            ],
        ):
            with mock.patch.object(socket, "getfqdn", return_value="test_host.local"):
                await idem_windows.corn.net.fqdn.load_fqdn(c_hub)

        assert c_hub.corn.CORN.fqdn == "test_host.local"
        assert c_hub.corn.CORN.host == "test_host"
        assert c_hub.corn.CORN.fqdn_ip4 == ("192.168.1.2",)
        assert c_hub.corn.CORN.fqdn_ip6 == ("fe80::71a9:ffff:ffff:ffff",)
        assert c_hub.corn.CORN.fqdns == ("192.168.1.2", "fe80::71a9:ffff:ffff:ffff")
