import idem_windows.corn.net.domain
import pytest
import win32net
import win32netcon
import unittest.mock as mock


class TestDomain:
    @pytest.mark.asyncio
    async def test_load_windows_domain(self, c_hub):
        with mock.patch.object(
            win32net,
            "NetGetJoinInformation",
            return_value=["testdomain", win32netcon.NetSetupDomainName,],
        ):
            await idem_windows.corn.net.domain.load_windows_domain(c_hub)

        assert c_hub.corn.CORN.windowsdomain == "testdomain"
        assert c_hub.corn.CORN.windowsdomaintype == "Domain"
