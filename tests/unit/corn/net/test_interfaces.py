import idem_windows.corn.net.interfaces
import pytest
from typing import List


class TestInterfaces:
    @pytest.mark.asyncio
    async def test_load_interfaces(self, c_hub):
        class Interface:
            description = "iface Desc"
            macaddress = "00:00:00:00:01"
            ipenabled = 1
            ipsubnet = {"255.255.255.0", "64"}
            up = True
            defaultipgateway = {"192.168.2.1", "fe80::2"}
            ipaddress = {"192.168.2.1", "fe80::2"}

        c_hub.exec.wmi.get.return_value = [Interface()]
        await idem_windows.corn.net.interfaces.load_interfaces(c_hub)

        assert c_hub.corn.CORN.ip4_gw == ("192.168.2.1",)
        assert c_hub.corn.CORN.ip6_gw == ("fe80::2",)
        assert c_hub.corn.CORN.ip_gw is True

        assert c_hub.corn.CORN.ipv4 == ("192.168.2.1",)
        assert c_hub.corn.CORN.ipv6 == ("fe80::2",)
        assert c_hub.corn.CORN.hwaddr_interfaces._dict() == {
            "iface Desc": "00:00:00:00:01"
        }
        assert c_hub.corn.CORN.ip4_interfaces._dict() == {
            "iface Desc": ("192.168.2.1",)
        }
        assert c_hub.corn.CORN.ip6_interfaces._dict() == {"iface Desc": ("fe80::2",)}
        assert c_hub.corn.CORN.ip_interfaces._dict() == {
            "iface Desc": ("192.168.2.1", "fe80::2")
        }

    @pytest.mark.asyncio
    async def test_load_gateway_false(self, c_hub):
        c_hub.exec.wmi.get.return_value = []
        await idem_windows.corn.net.interfaces.load_interfaces(c_hub)

        assert c_hub.corn.CORN.ip4_gw is False
        assert c_hub.corn.CORN.ip6_gw is False
        assert c_hub.corn.CORN.ip_gw is False

        assert c_hub.corn.CORN.ipv4 == ()
        assert c_hub.corn.CORN.ipv6 == ()
