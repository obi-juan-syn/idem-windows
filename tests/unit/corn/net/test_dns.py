import dns.resolver
import idem_windows.corn.net.dns
import pytest
import unittest.mock as mock


class TestDns:
    @pytest.mark.asyncio
    async def test_load_dns(self, c_hub):
        class resolver:
            domain = "lan."
            nameservers = ["192.168.1.1", "192.168.2.1", "::1", "fe80::1"]
            search = ["test"]

        with mock.patch.object(dns.resolver, "Resolver", return_value=resolver()):
            await idem_windows.corn.net.dns.load_dns(c_hub)

        assert c_hub.corn.CORN.dns.nameservers == (
            "192.168.1.1",
            "192.168.2.1",
            "::1",
            "fe80::1",
        )
        assert c_hub.corn.CORN.dns.ip4_nameservers == ("192.168.1.1", "192.168.2.1")
        assert c_hub.corn.CORN.dns.ip6_nameservers == ("::1", "fe80::1")
        assert c_hub.corn.CORN.dns.sortlist == (
            "192.168.1.0/24",
            "192.168.2.0/24",
            "::1/128",
            "fe80::1/128",
        )
        assert c_hub.corn.CORN.dns.domain == "lan."
        assert c_hub.corn.CORN.dns.search == ("test",)
        assert c_hub.corn.CORN.dns.options == {}
