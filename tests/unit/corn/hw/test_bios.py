import idem_windows.corn.hw.bios
import pytest


class TestBios:
    @pytest.mark.asyncio
    async def test_load_bios_info(self, c_hub):
        ret = lambda: 0
        ret.Name = "A.A0"
        ret.ReleaseDate = "20190618000000.000000+000"
        ret.SerialNumber = "TESTSERIALNUMBER"

        c_hub.exec.wmi.get.return_value = ret
        await idem_windows.corn.hw.bios.load_bios_info(c_hub)

        assert c_hub.corn.CORN.biosversion == "A.A0"
        assert c_hub.corn.CORN.biosreleasedate == "6/18/2019"
        assert c_hub.corn.CORN.serialnumber == "TESTSERIALNUMBER"
