import idem_windows.corn.hw.gpu
import pytest


class TestGpu:
    @pytest.mark.asyncio
    async def test_load_gpu_data(self, c_hub):
        ret = lambda: 0
        ret.Name = "testgpu"
        ret.AdapterCompatibility = "testvendor"

        ret1 = lambda: 0
        ret1.Name = "testgpu1"
        ret1.AdapterCompatibility = "testvendor1"

        c_hub.exec.wmi.get.return_value = [ret, ret1]
        await idem_windows.corn.hw.gpu.load_gpu_data(c_hub)

        assert c_hub.corn.CORN.gpus == (
            {"model": "testgpu", "vendor": "testvendor"},
            {"model": "testgpu1", "vendor": "testvendor1"},
        )
        assert c_hub.corn.CORN.num_gpus == 2
