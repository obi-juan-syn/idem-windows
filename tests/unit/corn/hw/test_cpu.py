import idem_windows.corn.hw.cpu
import os
import pytest
import unittest.mock as mock


class TestCpu:
    @pytest.mark.asyncio
    async def test_load_cpu_model(self, c_hub):
        ret = "Test Value"
        c_hub.exec.reg.read_value.return_value = {"vdata": ret}
        await idem_windows.corn.hw.cpu.load_cpu_model(c_hub)

        assert c_hub.corn.CORN.cpu_model == ret

    @pytest.mark.asyncio
    async def test_load_num_cpus(self, c_hub):
        with mock.patch.dict(os.environ, {"NUMBER_OF_PROCESSORS": "99999"}):
            await idem_windows.corn.hw.cpu.load_num_cpus(c_hub)

        assert c_hub.corn.CORN.num_cpus == 99999
