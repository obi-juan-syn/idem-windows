import idem_windows.corn.hw.platform
import platform
import pytest
import unittest.mock as mock


class TestPlatform:
    @pytest.mark.asyncio
    async def test_load_cpuarch(self, c_hub):
        with mock.patch.object(platform, "machine", return_value="test"):
            await idem_windows.corn.hw.platform.load_cpuarch(c_hub)

        assert c_hub.corn.CORN.cpuarch == "test"

    @pytest.mark.asyncio
    async def test_load_kernel_version(self, c_hub):
        with mock.patch.object(platform, "node", return_value="test"):
            await idem_windows.corn.hw.platform.load_nodename(c_hub)

        assert c_hub.corn.CORN.nodename == "test"

    @pytest.mark.asyncio
    async def test_load_nodename(self, c_hub):
        with mock.patch.object(platform, "version", return_value="test"):
            await idem_windows.corn.hw.platform.load_kernel_version(c_hub)

        assert c_hub.corn.CORN.kernelversion == "test"
