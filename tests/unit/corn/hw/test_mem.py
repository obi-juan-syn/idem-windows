import idem_windows.corn.hw.mem
import pytest
import unittest.mock as mock
import win32api


class TestMem:
    @pytest.mark.asyncio
    async def test_load_memdata(self, c_hub):
        with mock.patch.object(
            win32api, "GlobalMemoryStatusEx", return_value={"TotalPhys": 9999999999}
        ):
            await idem_windows.corn.hw.mem.load_memdata(c_hub)

        assert c_hub.corn.CORN.mem_total == 9536

    @pytest.mark.asyncio
    async def test_load_swapdata(self, c_hub):
        ret = lambda: 0
        ret.AllocatedBaseSize = 9999

        c_hub.exec.wmi.get.return_value = ret
        await idem_windows.corn.hw.mem.load_swapdata(c_hub)

        assert c_hub.corn.CORN.swap_total == 9999
