import idem_windows.corn.hw.virtualization
import pytest


class TestVirtualization:
    @pytest.mark.asyncio
    async def test_load_hw_virt_enabled(self, c_hub):
        c_hub.exec.wmi.get.return_value = True
        await idem_windows.corn.hw.virtualization.load_hw_virt_enabled(c_hub)

        assert c_hub.corn.CORN.hardware_virtualization is True
