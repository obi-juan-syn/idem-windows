import idem_windows.corn.hw.disks
import pytest

WMIC_DATA = """
DeviceId  MediaType
4         4
2         0
1         4
0         3
3         4
"""


class TestDisks:
    @pytest.mark.asyncio
    async def test_load_disks(self, c_hub):
        c_hub.exec.wmic.get.return_value = WMIC_DATA
        await idem_windows.corn.hw.disks.load_disks(c_hub)

        assert c_hub.corn.CORN.disks == (
            "\\\\.\\PhysicalDrive0",
            "\\\\.\\PhysicalDrive1",
            "\\\\.\\PhysicalDrive2",
            "\\\\.\\PhysicalDrive3",
            "\\\\.\\PhysicalDrive4",
        )
        assert c_hub.corn.CORN.SSDs == (
            "\\\\.\\PhysicalDrive1",
            "\\\\.\\PhysicalDrive3",
            "\\\\.\\PhysicalDrive4",
        )

    @pytest.mark.asyncio
    async def test_load_fibre_channel(self, c_hub):
        pass
        # TODO ONCE The grain works this test needs to be written

    @pytest.mark.asyncio
    async def test_load_iqn(self, c_hub):
        c_hub.exec.wmic.get.return_value = "iqn.test\niqn.test1\niqn.test2"
        await idem_windows.corn.hw.disks.load_iqn(c_hub)

        assert c_hub.corn.CORN.iscsi_iqn == ("iqn.test", "iqn.test1", "iqn.test2")
