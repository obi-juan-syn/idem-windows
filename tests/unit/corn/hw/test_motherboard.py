import idem_windows.corn.hw.motherboard
import pytest


class TestMotherboard:
    @pytest.mark.asyncio
    async def test_load_motherboard(self, c_hub):
        ret = lambda: 0
        ret.Product = "testproduct"
        ret.SerialNumber = "testserial"

        c_hub.exec.wmi.get.return_value = ret
        await idem_windows.corn.hw.motherboard.load_motherboard(c_hub)

        assert c_hub.corn.CORN.motherboard.productname == "testproduct"
        assert c_hub.corn.CORN.motherboard.serialnumber == "testserial"

    @pytest.mark.asyncio
    async def test_load_system_info(self, c_hub):
        ret = lambda: 0
        ret.Manufacturer = "Micro-Star International Co., Ltd."
        ret.Model = "TEST-1234"
        ret.Name = "TEST-NAME"

        c_hub.exec.wmi.get.return_value = ret
        await idem_windows.corn.hw.motherboard.load_system_info(c_hub)

        assert c_hub.corn.CORN.manufacturer == "Micro-Star International Co., Ltd."
        assert c_hub.corn.CORN.productname == "TEST-1234"
        assert c_hub.corn.CORN.computer_name == "TEST-NAME"
