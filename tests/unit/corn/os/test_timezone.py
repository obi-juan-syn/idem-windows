import idem_windows.corn.os.timezone
import pytest


class TestTimezone:
    @pytest.mark.asyncio
    async def test_load_timezone(self, c_hub):
        class Timeinfo:
            Description = "test_timezone"

        c_hub.exec.wmi.get.return_value = Timeinfo()
        await idem_windows.corn.os.timezone.load_timezone(c_hub)

        assert c_hub.corn.CORN.timezone == "test_timezone"
