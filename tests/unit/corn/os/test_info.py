import idem_windows.corn.os.info
import platform
import pytest
import unittest.mock as mock


class TestInfo:
    @pytest.mark.asyncio
    async def test_load_build(self, c_hub):
        c_hub.exec.reg.read_value.side_effect = [
            {"vdata": "100"},
            {"vdata": "111"},
        ]
        await idem_windows.corn.os.info.load_build(c_hub)

        assert c_hub.corn.CORN.osbuild == "100.111"

    @pytest.mark.asyncio
    async def test_load_build_version(self, c_hub):
        c_hub.exec.reg.read_value.return_value = {"vdata": "1801"}
        await idem_windows.corn.os.info.load_build_version(c_hub)
        assert c_hub.corn.CORN.osbuildversion == 1801

    @pytest.mark.asyncio
    async def test_load_codename(self, c_hub):
        c_hub.exec.reg.read_value.return_value = {"vdata": "bedrock"}
        await idem_windows.corn.os.info.load_codename(c_hub)
        assert c_hub.corn.CORN.oscodename == "bedrock"

    @pytest.mark.asyncio
    async def test_load_osinfo(self, c_hub):
        class osinfo:
            Manufacturer = "testman"
            Caption = "Windows Test Server 2099 R6"
            Version = "99.99.99"
            CSDVersion = "Service Pack 55"
            OSArchitecture = "testarch"
            ProductType = 0

        c_hub.exec.wmi.get.return_value = osinfo()
        await idem_windows.corn.os.info.load_osinfo(c_hub)

        assert c_hub.corn.CORN.os == "Windows"
        assert c_hub.corn.CORN.osmanufacturer == "testman"
        assert c_hub.corn.CORN.osfullname == "Windows Test Server 2099 R6"
        assert c_hub.corn.CORN.kernelrelease == "99.99.99"
        assert c_hub.corn.CORN.osversion == "99.99.99"
        assert c_hub.corn.CORN.osrelease == "2099ServerR6"
        assert c_hub.corn.CORN.osfinger == "Windows-2099ServerR6"
        assert c_hub.corn.CORN.osrelease_info == (99, 99, 99)
        assert c_hub.corn.CORN.osservicepack == "Service Pack 55"
        assert c_hub.corn.CORN.osarch == "testarch"

    @pytest.mark.asyncio
    async def test_load_osmajorrelease(self, c_hub):
        c_hub.exec.reg.read_value.return_value = {"vdata": "99"}
        await idem_windows.corn.os.info.load_osmajorrelease(c_hub)
        assert c_hub.corn.CORN.osmajorrelease == 99
