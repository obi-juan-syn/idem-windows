import idem_windows.corn.proc.user
import pytest


class TestUser:
    @pytest.mark.asyncio
    async def test_load_console_user(self, c_hub):
        class Systeminfo:
            UserName = "test_login_user"

        c_hub.exec.wmi.get.return_value = Systeminfo()
        c_hub.exec.user.sid_from_name.return_value = (
            "1-5-21-992878714-404123874-2616370337-2999"
        )

        await idem_windows.corn.proc.user.load_console_user(c_hub)

        assert c_hub.corn.CORN.console_username == "test_login_user"
        assert c_hub.corn.CORN.console_user == 2999

    @pytest.mark.asyncio
    async def test_load_user(self, c_hub):
        c_hub.exec.user.current.return_value = "test_user"
        c_hub.exec.user.groups.return_value = ["Administrators", "foo", "bar"]
        c_hub.exec.user.sid_from_name.side_effect = [
            "1-5-21-992878714-404123874-2616370337-2999",
            "S-1-5-32-544",
        ]

        await idem_windows.corn.proc.user.load_user(c_hub)

        assert c_hub.corn.CORN.username == "test_user"
        assert c_hub.corn.CORN.uid == 2999
        assert c_hub.corn.CORN.groupname == "Administrators"
        assert c_hub.corn.CORN.gid == 544
