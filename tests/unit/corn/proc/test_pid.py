import idem_windows.corn.proc.pid
import os
import pytest
import unittest.mock as mock


class TestPid:
    @pytest.mark.asyncio
    async def test_load_pid(self, c_hub):
        with mock.patch.object(os, "getpid", return_value=99999):
            await idem_windows.corn.proc.pid.load_pid(c_hub)
        assert c_hub.corn.CORN.pid == 99999
