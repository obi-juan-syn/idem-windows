import idem_windows.corn.system.shell
import os
import pytest
import unittest.mock as mock


class TestShell:
    @pytest.mark.asyncio
    async def test_load_shell(self, c_hub):
        ret = r"C:\Windows\system32\test_shell.exe"
        with mock.patch.dict(os.environ, {"COMSPEC": ret}):
            await idem_windows.corn.system.shell.load_shell(c_hub)
        assert c_hub.corn.CORN.shell == ret
