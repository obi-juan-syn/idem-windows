import idem_windows.corn.system.requirement_versions
import os
import pytest
import sys
import unittest.mock as mock


class TestRequirementVersions:
    @pytest.mark.asyncio
    async def test_load_pip_versions(self, c_hub):
        c_hub.exec.cmd.run.side_effect = [
            c_hub.pop.data.imap(
                {
                    "retcode": 0,
                    "stdout": "Pip data:\nRequires: corn taco cheese pizza\nOther stuff",
                }
            ),
            c_hub.pop.data.imap(
                {
                    "retcode": 0,
                    "stdout": "corn==0\ntaco==1\ncheese==git@example.com:/cheese#egg=cheese\npizza",
                }
            ),
        ]
        with mock.patch.object(
            os.path, "join", return_value="/nonexistent_path/foo/bar/taco"
        ):
            await idem_windows.corn.system.requirement_versions.load_pip_versions(c_hub)

        assert c_hub.corn.CORN.requirement_versions._dict() == {
            "corn": "0",
            "taco": "1",
            "cheese": "git@example.com:/cheese#egg=cheese",
            "pizza": None,
        }

    @pytest.mark.asyncio
    async def test_load_python_version(self, c_hub):
        prev = sys.path
        sys.version_info = (1, 2, 3)
        await idem_windows.corn.system.requirement_versions.load_python_version(c_hub)
        sys.executable = prev
        assert c_hub.corn.CORN.pythonversion == (1, 2, 3)
