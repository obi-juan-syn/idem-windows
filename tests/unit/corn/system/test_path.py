import idem_windows.corn.system.path
import os
import pytest
import sys
import unittest.mock as mock


class TestPath:
    @pytest.mark.asyncio
    async def test_load_cwd(self, c_hub):
        ret = "/etc/path/test"
        with mock.patch.object(os, "getcwd", return_value=ret):
            await idem_windows.corn.system.path.load_cwd(c_hub)
        assert c_hub.corn.CORN.cwd == ret

    @pytest.mark.asyncio
    async def test_load_executable(self, c_hub):
        prev = sys.executable
        sys.executable = "test_executable"
        await idem_windows.corn.system.path.load_executable(c_hub)
        sys.executable = prev
        assert c_hub.corn.CORN.pythonexecutable == "test_executable"

    @pytest.mark.asyncio
    async def test_load_path(self, c_hub):
        with mock.patch.dict(os.environ, {"PATH": "/test/path;/test/path2"}):
            await idem_windows.corn.system.path.load_path(c_hub)
        assert c_hub.corn.CORN.path == "/test/path;/test/path2"

    @pytest.mark.asyncio
    async def test_load_pythonpath(self, c_hub):
        prev = sys.path
        sys.path = ["pythonpath"]
        await idem_windows.corn.system.path.load_pythonpath(c_hub)
        sys.executable = prev
        assert c_hub.corn.CORN.pythonpath == ("pythonpath",)
