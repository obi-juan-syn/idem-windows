import idem_windows.corn.system.features
import pytest


class TestFeatures:
    @pytest.mark.asyncio
    async def test_load_features(self, c_hub):
        class Feature:
            Name = "Microsoft-Windows-Subsystem-Linux"
            InstallState = 1

        c_hub.exec.wmi.get.return_value = [Feature()]

        await idem_windows.corn.system.features.load_features(c_hub)

        assert c_hub.corn.CORN.wsl is True
