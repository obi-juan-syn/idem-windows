import idem_windows.corn.system.locale
import locale
import pytest
import sys
import unittest.mock as mock


class TestLocale:
    @pytest.mark.asyncio
    async def test_load_locale(self, c_hub):
        with mock.patch.object(
            locale, "getdefaultlocale", return_value=("test_language", "test_encoding")
        ):
            with mock.patch.object(
                sys, "getdefaultencoding", return_value="test_detected_encoding"
            ):
                await idem_windows.corn.system.locale.load_locale(c_hub)

        assert c_hub.corn.CORN.locale_info.defaultlanguage == "test_language"
        assert c_hub.corn.CORN.locale_info.defaultencoding == "test_encoding"
        assert c_hub.corn.CORN.locale_info.detectedencoding == "test_detected_encoding"
        assert c_hub.corn.CORN.locale_info.detectedencoding
