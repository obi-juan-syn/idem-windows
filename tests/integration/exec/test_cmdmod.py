import asyncio
import collections.abc as abc
import os
import pytest


@pytest.mark.asyncio
class TestCmd:
    async def test_run(self, hub):
        ret = await hub.exec.cmd.run("dir")
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout.strip().split(), "No output from command"
        assert not ret.stderr

    async def test_run_error(self, hub):
        try:
            await hub.exec.cmd.run("nonexistent_command")
        except FileNotFoundError:
            return
        assert False, "Command execution should have failed"

    async def test_run_string_list(self, hub):
        ret = await hub.exec.cmd.run("ls -la")
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout, "No output from command"
        assert not ret.stderr

    async def test_run_list(self, hub):
        ret = await hub.exec.cmd.run(["ls", "-la"])
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout, "No output from command"
        assert not ret.stderr

    async def test_shell(self, hub):
        ret = await hub.exec.cmd.run("dir", shell=True)
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout, "No output from command"
        assert not ret.stderr

    async def test_run_cwd(self, hub):
        # Running with %cd% only works if shell=True
        cwd = os.getcwd()
        ret = await hub.exec.cmd.run(["echo", "%CD%"], cwd=cwd, shell=True)
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout.strip() == cwd
        assert not ret.stderr

    async def test_run_cwd_fail(self, hub):
        try:
            await hub.exec.cmd.run(["dir"], cwd="nonexistant_path")
        except SystemError:
            return
        assert False, "Expected a system error with a pad path"

    async def test_run_env(self, hub):
        env = {"test_env_attr": "test_env_val", "another": "value"}
        ret = await hub.exec.cmd.run("env", env=env)
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout.strip().startswith(
            "\n".join(f"{k}={v}" for k, v in env.items())
        )
        assert not ret.stderr

    async def test_run_timeout(self, hub):
        ret = await hub.exec.cmd.run("dir", timeout=1)
        assert isinstance(ret, abc.MutableMapping)
        assert ret.keys() == {"pid", "retcode", "stdout", "stderr"}
        assert ret.retcode == 0
        assert ret.stdout.strip(), "No output from command"
        assert not ret.stderr

    async def test_run_timeout_fail(self, hub):
        pytest.skip(
            "TODO This test passes but causes a ValueError with an I/O operation on a closed pipe"
        )
        try:
            await hub.exec.cmd.run(["sleep", "5"], timeout=0)
        except asyncio.TimeoutError:
            return

        assert False, "Timeout should have been reached"
