import asyncio
import ctypes
import pop.hub
import pytest
import sys
import unittest.mock as mock


@pytest.fixture(scope="session")
def hub():
    """
    provides a full hub that is used as a reference for mock_hub
    """
    hub = pop.hub.Hub()

    # strip pytest args
    with mock.patch.object(sys, "argv", sys.argv[:1]):
        hub.pop.sub.add(dyne_name="corn")
        hub.pop.sub.add(dyne_name="exec")
        hub.pop.sub.add(dyne_name="states")

    hub.corn.init.standalone()

    return hub


@pytest.fixture(scope="session")
def event_loop(hub):
    """
    Override the pytest-asyncio loop with the ProactorEventLoop on the hub
    """
    loop = hub.pop.Loop
    yield loop


def pytest_addoption(parser: argparse.Parser):
    parser.addoption("--expensive", action="store_true")


def pytest_configure(config: conf.Config):
    config.addinivalue_line("markers", "admin(): Test must be run as an administrator")
    config.addinivalue_line("markers", "expensive(): This test is expensive")


def pytest_collection_modifyitems(items, config: conf.Config):
    # Remove the expensive marker if the expensive flag was set
    if config.getoption("expensive"):
        for item in items:
            expensive = list(item.iter_markers(name="expensive"))
            if expensive:
                item.own_markers.remove(expensive[0])


def pytest_runtest_setup(item: pytest.Function):
    if (
        list(item.iter_markers(name="admin"))
        and not ctypes.windll.shell32.IsUserAnAdmin()
    ):
        pytest.skip("Test must be run as an administrator")

    if list(item.iter_markers(name="expensive")):
        pytest.skip("Expensive tests are disabled")
