import pytest


class TestCorn:
    @pytest.mark.asyncio
    async def test_corn(self, hub):
        """
        Verify that a standard set of grains have been defined
        """
        missing_grains = {
            "biosreleasedate",
            "biosversion",
            "computer_name",
            "console_user",
            "console_username",
            "cpu_model",
            "cpuarch",
            "cwd",
            "disks",
            "dns",
            "domain",
            "fqdn",
            "fqdn_ip4",
            "fqdn_ip6",
            "fqdns",
            "gid",
            "gpus",
            "groupname",
            "hardware_virtualization",
            "host",
            "hwaddr_interfaces",
            "init",
            "ip4_gw",
            "ip4_interfaces",
            "ip6_gw",
            "ip6_interfaces",
            "ip_gw",
            "ip_interfaces",
            "ipv4",
            "ipv6",
            "kernel",
            "kernelrelease",
            "kernelversion",
            "locale_info",
            "localhost",
            "manufacturer",
            "mem_total",
            "motherboard",
            "nodename",
            "num_cpus",
            "num_gpus",
            "os",
            "os_family",
            "osarch",
            "osbuild",
            "osbuildversion",
            "oscodename",
            "osfinger",
            "osfullname",
            "osmajorrelease",
            "osmanufacturer",
            "osrelease",
            "osrelease_info",
            "osservicepack",
            "osversion",
            "path",
            "pid",
            "productname",
            "ps",
            "pythonexecutable",
            "pythonpath",
            "pythonversion",
            "requirement_versions",
            "saltpath",
            "saltversion",
            "saltversioninfo",
            "serialnumber",
            "SSDs",
            "shell",
            "swap_total",
            "timezone",
            "uid",
            "username",
            "windowsdomain",
            "windowsdomaintype",
            "wsl",
        } - set(hub.corn.CORN.keys())
        assert not missing_grains, "Essential grains are missing: {}".format(
            ", ".join(missing_grains)
        )

    @pytest.mark.asyncio
    async def test_corn_values(self, hub, subtests):
        """
        Verify that all corns have values
        """
        for grain, value in hub.corn.CORN.items():
            with subtests.test(grain=grain):
                if value is None:
                    pytest.fail(f'"{grain}" was not assigned')
                elif not (value or isinstance(value, int) or isinstance(value, bool)):
                    pytest.skip(f'"{grain}" does not have a value')
